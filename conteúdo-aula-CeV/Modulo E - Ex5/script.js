function tabuada(){
    var mult = document.body.querySelector("input#num").value;
    var tab = document.querySelector("select#res")
    
    if (mult.length == 0){
        window.alert("Por favor digite um número");
    }else{
        tab.innerHTML = ''
        for (var i=1; i<=10; i++){
            var res = document.createElement("option")
            res.text = `${mult} x ${i} = ${i*Number(mult)}`
            res.value = `tab${i}`
            tab.appendChild(res)
        }
    }
}