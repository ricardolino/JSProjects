function verificar(){
    var ano = new Date().getFullYear();
    var fano = document.querySelector('input#txtano');
    var res = document.getElementById('res');

    if (Number(fano.value.length) == 0 || Number(fano.value) > ano){
        alert('[ERRO] Verifique os dados e tente novamente');
    }else{
        var fsex = document.getElementsByName('radsex');
        var idade = ano - Number(fano.value);
        var genero = '';
        var img = document.createElement('img');
        img.setAttribute('id', 'foto');

        if (fsex[0].checked){
            genero = 'Homem';

            if (idade >=0 && idade < 10){
                //Criança masculino
                img.setAttribute('src', 'Imagens/foto-bebe-m.png');
            }else if (idade < 21){
                //Jovem masculino
                img.setAttribute('src', 'Imagens/foto-jovem-m.png');
            }else if (idade < 50){
                //Adulto masculino
                img.setAttribute('src', 'Imagens/foto-adulto-m.png');
            }else{
                //Idoso masculino
                img.setAttribute('src', 'Imagens/foto-idoso-m.png');
            }

        }else{
            genero = 'Mulher';

            if (idade >= 0 && idade < 10){
                //Criança feminino
                img.setAttribute('src', 'Imagens/foto-bebe-f.png');
            }else if (idade < 21){
                //Jovem feminino
                img.setAttribute('src', 'Imagens/foto-jovem-f.png');
            }else if (idade < 50){
                //Adulto fenimino
                img.setAttribute('src', 'Imagens/foto-adulto-f.png');
            }else{
                //Idoso feminino
                img.setAttribute('src', 'Imagens/foto-idoso-f.png');
            }

        }

        res.style.textAlign = 'center';
        res.innerHTML = `Detectamos ${genero} com ${idade} anos.`;
        res.appendChild(img)
    }
}