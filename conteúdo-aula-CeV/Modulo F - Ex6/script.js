var lista = [];

function adicionar_num(){
    var num = Number.parseInt(document.getElementById('recebe_num').value);

    if (1 <= num && num <= 100){
        if (lista.indexOf(num) == -1){
            lista.push(num);
            var res = document.createElement('option');
            res.text = `Valor ${num} adicionado`;
            document.getElementById('seleção_adicionados').appendChild(res);
            document.getElementById('resultado').innerHTML = '';
        }else{
            alert('Número já está na lista');
        }
    }else{
        alert('Entrada inválido, digite um número entre 1 e 100');
    }

    document.getElementById('recebe_num').value = ''
    document.getElementById('recebe_num').focus()
}

function mostrar_resultado(){
    if(lista.length > 0){
        var maior = 0;
        var menor = 101;
        var soma = 0;

        for (var i = 0; i < lista.length; i++){
            if (lista[i] > maior){
                maior = lista[i];
            }
            if (lista[i] < menor){
                menor = lista[i];
            }
            soma += lista[i];
        }

        document.getElementById('resultado').innerHTML = `<p>Ao todo temos ${lista.length} cadastrados.</p>
        <p>O maior valor informado foi ${maior}.</p>
        <p>O menor valor informado foi ${menor}.</p>
        <p>Somando todos o valores, temos ${soma}.</p>
        <p>A média dos valores digitados é ${(soma/lista.length).toFixed(2)}.</p>`;
    }else{
        alert('Adicione valores antes de Finalizar.')
    }
}
