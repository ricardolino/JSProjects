function gerar_loop(){
    var inicio = document.body.querySelector("input#inicio").value;
    var fim = document.body.querySelector("input#fim").value;
    var passo = document.body.querySelector("input#passo").value;
    var res = document.querySelector("div#res");
    var i;

    if (Number(inicio) == Number(fim) || inicio.length == 0 || fim.length == 0){
        window.alert("Favor preencha os campos corrretamente");
    }else{
        if (Number(passo) <= 0){
            window.alert("Passo inválido, condiderando passo 1")
            passo = 1
        }
        res.innerHTML = "Contador: ";
        if (Number(fim) > Number(inicio)){
            for(i=Number(inicio); i<=Number(fim); i+=Number(passo)){
                res.innerHTML += `${i} \u{1F449} `;
            } 
        } else if (Number(inicio) > Number(fim)){
            for (i=Number(inicio); i>=Number(fim); i=i-Number(passo)){
                res.innerHTML += `${i} \u{1F449} `;
            }
        }
        res.innerHTML += `\u{1F3C1}`
    }
}